package com.productsir.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * Created by kevenxia on 2017/01/15.
 */
@Entity
@AllArgsConstructor
@Data
@Table(name = "MmWeChatGroup")
public class WechatGroup {
    @Id
    @Column(name = "idMmGroup")
    @GeneratedValue
    private Long id;

    @Column(name = "Name")
    private String name;
    @OneToMany
    @JoinColumn(name = "idMmGroup", referencedColumnName = "idMmGroup")
    private List<WechatUser> users;
}
