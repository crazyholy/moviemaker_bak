package com.productsir.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

/**
 * Created by kevenxia on 2017/01/09.
 */
@Entity
@Table(name = "MmUser")
@AllArgsConstructor
@Data
public class User {
    @Column(name = "idMmUser")
    @Id
    @GeneratedValue
    private long id;
    @Column(name = "Name")
    private String name;
    @Column(name = "Password")
    private String password;
    @Column(name = "RealName")
    private String realName;
    @Column(name = "AvatarUrl")
    private String avatarUrl;
}

