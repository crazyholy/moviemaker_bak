package com.productsir.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * Created by kevenxia on 2017/01/15.
 */
@Entity
@Table(name = "MmNoticeEx")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NoticeEx {
    @Id
    @Column(name = "idMmNotice")
    @GeneratedValue
    private int id;
    @OneToOne
    @Basic(fetch = FetchType.LAZY)
    private Project project;
    @OneToOne
    @Basic(fetch = FetchType.LAZY)
    @JoinColumn(name = "idUser")
    private User user;
    @Column(name = "Title")
    private String title;
    @Column(name = "DayInfo")
    private String dayInfo;
    @Column(name = "DayInfoColor")
    private String dayInfoColor;
    @Column(name = "DayProcessInfo")
    private String dayProcessInfo;
    @Column(name = "DayProcessInfoColor")
    private String dayProcessInfoColor;
    @Column(name = "WeatherInfo")
    private String weatherInfo;
    @Column(name = "WeatherInfoColor")
    private String weatherInfoColor;
    @Column(name = "ScheduleInfo")
    private String scheduleInfo;
    @Column(name = "ScheduleInfoColor")
    private String scheduleInfoColor;
    @Column(name = "TeamInfo")
    private String teamInfo;
    @Column(name = "TeamInfoColor")
    private String teamInfoColor;
    @OneToMany
    @JoinColumn(name = "idMmNotice", referencedColumnName = "idMmNotice")
    private List<NoticeActorEx> actors;
    @OneToMany
    @JoinColumn(name = "idMmNotice", referencedColumnName = "idMmNotice")
    private List<SceneEx> scenes;
    @OneToMany
    @JoinColumn(name = "idMmNotice", referencedColumnName = "idNotice")
    private List<Marks> marks;
}
