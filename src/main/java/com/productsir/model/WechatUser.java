package com.productsir.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

/**
 * Created by kevenxia on 2017/01/15.
 */
@Entity
@Table(name = "MmWeChatUser")
@AllArgsConstructor
@Data
public class WechatUser {
    @Column(name = "idMmWeChatUser")
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "NickName")
    private String nickName;
    @Column(name = "OpenID")
    private String openId;
}
