package com.productsir.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by kevenxia on 2017/01/15.
 */
@Entity
@Table(name = "MmMarksEx")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Marks {
    @Id
    @GeneratedValue
    @Column(name = "idMarks")
    private Long id;
    @Column(name = "Content")
    private String content;
    @Column(name = "Color")
    private String color;
    @ManyToOne
    @JoinColumn(name = "idNotice", referencedColumnName = "idMmNotice")
    private NoticeEx notice;
}
