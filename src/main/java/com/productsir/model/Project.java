package com.productsir.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by kevenxia on 2017/01/15.
 */
@Entity
@AllArgsConstructor
@Data
@Table(name = "MmProject")
public class Project {
}
