package com.productsir.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * Created by kevenxia on 2017/01/15.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "MmSceneEx")
public class SceneEx {
    @Id
    @Column(name = "idMmScene")
    private Long id;
    @Column(name = "No")
    private String no;
    @Column(name = "SceneNote")
    private String sceneNote;
    @Column(name = "SceneNoTitle")
    private String sceneNoTitle;
    @Column(name = "Title")
    private String title;
    @Column(name = "TitleColor")
    private String titleColor;
    @Column(name = "Content")
    private String content;
    @Column(name = "ContentColor")
    private String contentColor;
    @Column(name = "Light")
    private String light;
    @Column(name = "LightColor")
    private String lightColor;
    @Column(name = "Side")
    private String side;
    @Column(name = "SideColor")
    private String sideColor;
    @Column(name = "SpecialEffect")
    private String specialEffect;
    @Column(name = "SpecialEffectColor")
    private String specialEffectColor;
    @Column(name = "PageCount")
    private String pageCount;
    @Column(name = "PageCountColor")
    private String pageCountColor;
    @Column(name = "SpecialProp")
    private String specialProp;
    @Column(name = "SpecialPropColor")
    private String specialPropColor;
    @Column(name = "Extras")
    private String extras;
    @Column(name = "ExtrasColor")
    private String extrasColor;
    @Column(name = "StandInComment")
    private String standInComment;
    @Column(name = "StandInCommentColor")
    private String standInCommentColor;

    @OneToMany
    @JoinColumn(name = "idMmScene", referencedColumnName = "idMmScene")
    private List<SceneActorEx> sceneActors;
}
