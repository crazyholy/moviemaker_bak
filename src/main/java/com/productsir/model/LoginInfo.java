package com.productsir.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by kevenxia on 2017/01/15.
 */
@Entity
@Data
@Table(name = "MmLogin")
@AllArgsConstructor
public class LoginInfo {
    @Column(name = "Token")
    private String token;
    @Column(name = "Device")
    private String device;
    @Column(name = "IpAddress")
    private String ipAddress;
    @Column(name = "LoginTime")
    private Date loginTime;
    @Column(name = "ExpireTime")
    private Date expireTime;
    @OneToOne
    @JoinColumn(name = "idMmUser", referencedColumnName = "idMmUser")
    private User user;
}
