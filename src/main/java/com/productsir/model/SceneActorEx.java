package com.productsir.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by kevenxia on 2017/01/15.
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MmSceneActorEx")
@Data
public class SceneActorEx {
    @Id
    @GeneratedValue
    @Column(name = "idMmSceneActor")
    private Long id;
    @Column(name = "Name")
    private String name;
    @Column(name = "Color")
    private String color;
    @Column(name = "idMmScene")
    private Long idScene;
}
