package com.productsir.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by kevenxia on 2017/01/15.
 */
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "MmNoticeActors")
public class NoticeActorEx {
    @Id
    @Column(name = "idMmNotice")
    private Long id;
    @Id
    @Column(name = "Name")
    private String name;
    @Id
    @Column(name = "ActorName")
    private String actorName;
    @Column(name = "MakeStartTime")
    private String makeStartTime;
    private String makeEndTime;
}
