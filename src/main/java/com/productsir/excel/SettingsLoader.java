package com.productsir.excel;

import com.productsir.excel.model.NoticeSettings;
import lombok.Cleanup;
import lombok.SneakyThrows;
import lombok.experimental.var;
import org.yaml.snakeyaml.Yaml;

import java.io.FileInputStream;

/**
 * Created by kevenxia on 2017/01/15.
 */
public final class SettingsLoader {
    private static  SettingsLoader loader = new SettingsLoader();
    private NoticeSettings settings;

    private  SettingsLoader() {
    }

    public static SettingsLoader getLoader() {
        return loader;
    }

    @SneakyThrows
    public void load(String file) {
        @Cleanup
        var input = new FileInputStream(file);
        var yaml = new Yaml();

        settings = yaml.loadAs(input, NoticeSettings.class);
    }

    public NoticeSettings getSettings() {
        return settings;
    }
}
