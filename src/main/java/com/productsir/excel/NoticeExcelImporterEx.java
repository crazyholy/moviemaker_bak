package com.productsir.excel;

import com.productsir.excel.model.ImportResult;
import com.productsir.model.NoticeActorEx;
import com.productsir.model.NoticeEx;
import com.productsir.model.SceneActorEx;
import com.productsir.model.SceneEx;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by pengxia on 2017/02/15.
 */
@Slf4j
public class NoticeExcelImporterEx {
    private static final int RoleNameRow = 2 - 1;
    private static final int ActorNameRow = 3 - 1;
    private static final int MakeStartTimeRow = 4 - 1;
    private static final int MakeEndTimeRow = 5 - 1;
    private static final int SceneStartRow = 6 - 1;
    private static final int SceneStartColumn = 3 - 1;
    private static final int SceneTitleRow = 4 - 1;
    private static final int ActorStartColumn = 15 - 1;

    private String filePath;
    public NoticeExcelImporterEx(String filePath) {
        this.filePath = filePath;
    }

    public ImportResult execute() {
        val result = new ImportResult();
        Workbook workbook = null;
        result.setStatus(false);

        try {
            workbook = WorkbookFactory.create(new File(filePath));
            val notice = new NoticeEx();
            val sheet = workbook.getSheetAt(0);
            val rowAbstract = sheet.getRow(1);
            val cellDayInfo = rowAbstract.getCell(2);
            val dayInfo = cellDayInfo.getStringCellValue();
            // 时间信息
            val dayInfos = dayInfo.split("\n", -1);
            // 星期与进度信息
            val weakInfos = dayInfos[0].split("\\s+", -1);
            notice.setDayInfo(weakInfos[0] + " " + weakInfos[1]);
            notice.setDayProcessInfo(weakInfos[2]);
            // 天气信息
            val weatherInfos = dayInfos[1].split("\\s+", -1);
            notice.setWeatherInfo(String.join("\n", weatherInfos));
            notice.setScheduleInfo(rowAbstract.getCell(5).getStringCellValue());
            notice.setTeamInfo(rowAbstract.getCell(12).getStringCellValue());

            // 角色
            val rowActorName = sheet.getRow(ActorNameRow);
            val rowStartMakeTime = sheet.getRow(MakeStartTimeRow);
            val rowEndMakeTime = sheet.getRow(MakeEndTimeRow);

            val actors = new ArrayList<NoticeActorEx>();
            int currentColumn = 14;
            Cell cellActor = rowAbstract.getCell(currentColumn);
            while (!"群众演员".equals(cellActor.getStringCellValue())) {
                val actor = new NoticeActorEx();
                actor.setName(cellActor.getStringCellValue());

                val cellActorName = rowActorName.getCell(currentColumn);
                actor.setActorName(cellActorName.getStringCellValue());

                val cellStartMakeTime = rowStartMakeTime.getCell(currentColumn);
                actor.setMakeStartTime(cellStartMakeTime.getStringCellValue());

                val cellEndMakeTime = rowEndMakeTime.getCell(currentColumn);
                actor.setMakeEndTime(cellEndMakeTime.getStringCellValue());

                actors.add(actor);
                currentColumn++;
                cellActor = rowAbstract.getCell(currentColumn);
            }
            notice.setActors(actors);

            // 场序
            val rowSceneTitle = sheet.getRow(SceneTitleRow);
            Row rowScene = sheet.getRow(SceneStartRow);
            Cell cellScene = rowScene.getCell(SceneStartColumn);

            val actorCount = actors.size();
            val scenes = new ArrayList<SceneEx>();
            int currentRow = SceneStartRow;
            currentColumn = SceneStartColumn;
            while (true) {
                val no = cellScene.getStringCellValue();
                try {
                    Integer.parseInt(no);
                } catch (NumberFormatException ex) {
                    break;
                }

                val scene = new SceneEx();
                scene.setNo(no);
                scene.setSceneNote(rowScene.getCell(++currentColumn).getStringCellValue());
                scene.setSceneNoTitle(rowScene.getCell(++currentColumn).getStringCellValue());
                scene.setTitle(rowScene.getCell(++currentColumn).getStringCellValue());
                scene.setContent(rowScene.getCell(++currentColumn).getStringCellValue());
                scene.setLight(rowScene.getCell(++currentColumn).getStringCellValue());
                scene.setSide(rowScene.getCell(++currentColumn).getStringCellValue());
                scene.setSpecialEffect(rowScene.getCell(++currentColumn).getStringCellValue());
                scene.setPageCount(rowScene.getCell(++currentColumn).getStringCellValue());
                scene.setStandInComment(rowScene.getCell(++currentColumn).getStringCellValue());
                scene.setSpecialProp(rowScene.getCell(++currentColumn).getStringCellValue());
                val sceneActors = new ArrayList<SceneActorEx>();
                for (int i = 0; i < actorCount; i++) {
                    if (rowScene.getCell(ActorStartColumn + i).getStringCellValue().isEmpty()) {
                        continue;
                    }

                    val sceneActor = new SceneActorEx();
                    val cellSceneActor = rowAbstract.getCell(ActorStartColumn + i);
                    sceneActor.setName(cellSceneActor.getStringCellValue());

                    sceneActors.add(sceneActor);
                }
                scene.setSceneActors(sceneActors);

                scenes.add(scene);
                currentRow++;
                currentColumn = SceneStartColumn;
                rowScene = sheet.getRow(currentRow);
                cellScene = rowScene.getCell(currentColumn);
            }

            notice.setScenes(scenes);

            result.setStatus(true);
        } catch (Exception ex) {
            log.error("Can not import workbook, {}", filePath, ex);

            result.setMessage(ex.getMessage());
        } finally {
            if (workbook != null)
                try {
                    workbook.close();
                } catch (IOException e) {
                    log.warn("无法关闭Workbook", e);
                }
        }

        return result;
    }
}
