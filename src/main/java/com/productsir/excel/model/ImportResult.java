package com.productsir.excel.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by kevenxia on 2017/01/15.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ImportResult {
    private boolean status;
    private String message;
}
