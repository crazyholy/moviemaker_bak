package com.productsir.excel.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by kevenxia on 2017/01/15.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ModelSettings {
    private List<CellSetting> settings;
}

