package com.productsir.excel.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by kevenxia on 2017/01/15.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CellSetting {
    private int row;
    private int column;
    private String target;
    private String colorTarget;
}
