package com.productsir.excel;

import com.productsir.excel.model.ImportException;
import com.productsir.excel.model.ImportResult;
import com.productsir.excel.model.ModelSettings;
import com.productsir.model.NoticeEx;
import lombok.SneakyThrows;
import lombok.experimental.var;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFFont;

import javax.persistence.Table;
import java.io.File;
import java.io.IOException;

/**
 * Created by kevenxia on 2017/01/15.
 */
@Slf4j
public class NoticeExcelImporter {
    private String filePath;

    public NoticeExcelImporter(String filePath) {
        this.filePath = filePath;
    }

    public ImportResult start() {
        var result = new ImportResult();
        Workbook workbook = null;
        result.setStatus(false);

        try {
            workbook = WorkbookFactory.create(new File(filePath));
            var sheet = workbook.getSheetAt(0);

            var notice = new NoticeEx();
            var noticeSettings = SettingsLoader.getLoader().getSettings();

            if (noticeSettings == null) {
                result.setMessage("通告单解析配置文件读取失败，无法解析通告单。");
                return result;
            }

            extraData(workbook, sheet, noticeSettings.getNotice(), notice);

        } catch (Exception ex) {
            log.error("通告单解析失败=> [file={}, error={}]", filePath, ex.getMessage(), ex);
            result.setMessage(ex.getMessage());

            return result;
        } finally {
            if (workbook != null)
                try {
                    workbook.close();
                } catch (IOException e) {
                    // ignore
                }
        }

        return result;
    }

    @SneakyThrows
    public void extraData(final Workbook book, final Sheet sheet, ModelSettings settings, Object obj) {
        var cls = obj.getClass();
        var table = cls.getAnnotation(Table.class);
        if (table == null) {
            throw new ImportException("解析无法完成，致命错误！");
        }

        for (var setting :
                settings.getSettings()) {
            var field = cls.getDeclaredField(setting.getTarget());
            if (field == null) {
                log.warn("无法找到指定设定字段：[Setting={}]", setting);
                continue;
            }

            field.setAccessible(true);
            var cell = sheet.getRow(setting.getRow()).getCell(setting.getColumn());
            field.set(obj, cell.getStringCellValue());

            var colorField = cls.getDeclaredField(setting.getColorTarget());
            if (colorField == null) {
                log.info("无颜色设定字段：[Setting={}", setting);
                continue;
            }
            colorField.setAccessible(true);
            var style = cell.getCellStyle();
            var font = book.getFontAt(style.getFontIndex());
            var colorIndex = font.getColor();
            if (font instanceof HSSFFont) {
                if (colorIndex != HSSFFont.COLOR_NORMAL) {
                    var color = ((HSSFWorkbook)book).getCustomPalette().getColor(colorIndex);
                    if (color != null) {
                        colorField.set(obj, color.getHexString());
                    }
                }
            } else if (font instanceof XSSFFont) {
                var xssfFont = (XSSFFont)font;
                var color = xssfFont.getXSSFColor();

                if (color != null) {
                    colorField.set(obj, color.getARGBHex());
                }
            }
        }
    }

}
