package com.productsir.dao;

import com.productsir.model.User;
import org.springframework.data.repository.Repository;

import java.util.Optional;

/**
 * Created by kevenxia on 2017/01/15.
 */
public interface UserRepository extends Repository<User, Long> {
    /**
     * Get user by user name and password
     * @param name user name
     * @param password password
     * @return
     * user the user whose name matching the name and password
     * null no user whose name matching the name and password
     */
    Optional<User> findByNameAndPassword(String name, String password);

    /**
     * Save user persistence
     * @param user
     * @return
     */
    User save(User user);

    Optional<User> findOne(Long id);
}
